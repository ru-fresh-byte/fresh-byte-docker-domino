#!/bin/bash

software_repository_start ()
{
  STATUS="$(docker inspect --format '{{ .State.Status }}' $SOFTWARE_REPOSITORY_CONTAINER_NAME 2>/dev/null)"

  if [ -z "$STATUS" ]; then
    docker run --name $SOFTWARE_REPOSITORY_CONTAINER_NAME -p $SOFTWARE_REPOSITORY_PORT:80 -v $SOFTWARE_REPOSITORY_PATH:/usr/share/nginx/html:ro -d nginx 1>/dev/null
  elif [ "$STATUS" = "exited" ]; then
    docker start $SOFTWARE_REPOSITORY_CONTAINER_NAME 1>/dev/null
  fi

  
  SOFTWARE_REPOSITORY_IP="$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' $SOFTWARE_REPOSITORY_CONTAINER_NAME 2>/dev/null)"
  if [ -z "$SOFTWARE_REPOSITORY_IP" ]; then
    echo "Unable to locate software repository."
  else
    SOFTWARE_REPOSITORY_URL=http://$SOFTWARE_REPOSITORY_IP #:$SOFTWARE_REPOSITORY_PORT
    echo "Hosting software repository on $SOFTWARE_REPOSITORY_URL"
  fi
  echo
}

software_repository_stop ()
{
  docker stop $SOFTWARE_REPOSITORY_CONTAINER_NAME 1>/dev/null
  docker container rm $SOFTWARE_REPOSITORY_CONTAINER_NAME 1>/dev/null
  echo "Stopped & Removed Software Repository Container"
  echo
}

SCRIPT_NAME=$0
SCRIPT_DIR=`dirname $SCRIPT_NAME`


SOFTWARE_REPOSITORY_CONTAINER_NAME=software
SOFTWARE_REPOSITORY_PATH=/software
SOFTWARE_REPOSITORY_IP=127.0.0.1
SOFTWARE_REPOSITORY_PORT=8080
SOFTWARE_REPOSITORY_URL=http://$SOFTWARE_REPOSITORY_IP


DOCKER_IMAGE_NAME="freshbyte/domino"
DOCKER_IMAGE_VERSION="10.0.1FP6"
DOCKER_IMAGE_TAG=$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_VERSION
DOCKER_IMAGE_DESCRIPTION="HCL Domnio application server for FByte.Platform"
DOCKER_IMAGE_BUILD_TIME=`date +"%d.%m.%Y %H:%M:%S"`
DOCKER_IMAGE_BUILD_VERSION=$DOCKER_IMAGE_VERSION

software_repository_start

BUILD_ARG_SOFTWARE_REPOSITORY_URL="--build-arg SOFTWARE_REPOSITORY_URL=$SOFTWARE_REPOSITORY_URL"

pushd .
CURRENT_DIR=`dirname $SCRIPT_NAME`
cd $CURRENT_DIR

docker build --no-cache \
  --label "version"="$DOCKER_IMAGE_VERSION" \
  --label "buildtime"="$DOCKER_IMAGE_BUILD_TIME" \
  --label "description"="$DOCKER_IMAGE_DESCRIPTION" \
  -t $DOCKER_IMAGE_TAG \
  $BUILD_ARG_SOFTWARE_REPOSITORY_URL \
  .

popd
echo

software_repository_stop