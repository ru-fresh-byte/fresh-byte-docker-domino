FROM centos:7

LABEL FByteDominoDocker.maintainer="rombs@yandex.ru"

ARG SOFTWARE_REPOSITORY_URL=

EXPOSE 1352 80

USER root

COPY fbyte /fbyte

RUN yum install -y openssl bc which wget nano unzip rsync lsof gdb sysstat file net-tools perl && \
    /fbyte/docker/domino/install_domino.sh

ENTRYPOINT ["/domino_docker_entrypoint.sh"]

USER notes