#!/bin/bash

pushd()
{
  command pushd "$@" > /dev/null
}

popd ()
{
  command popd "$@" > /dev/null
}

log_error ()
{
  echo "[ERROR]: $1"
}

log_info ()
{
  echo "[INFO]:  $1"
}

progressfilter ()
{
    local flag=false c count cr=$'\r' nl=$'\n'
    while IFS='' read -d '' -rn 1 c
    do
        if $flag
        then
            printf '%s' "$c"
        else
            if [[ $c != $cr && $c != $nl ]]
            then
                count=0
            else
                ((count++))
                if ((count > 1))
                then
                    flag=true
                fi
            fi
        fi
    done
}

create_directory ()
{
  TARGET_FILE=$1
  OWNER=$2
  GROUP=$3
  PERMS=$4

  if [ -z "$TARGET_FILE" ]; then
    return 0
  fi

  if [ -e "$TARGET_FILE" ]; then
    return 0
  fi
  
  mkdir -p "$TARGET_FILE"
  
  if [ ! -z "$OWNER" ]; then
    chown $OWNER:$GROUP "$TARGET_FILE"
  fi

  if [ ! -z "$PERMS" ]; then
    chmod "$PERMS" "$TARGET_FILE"
  fi
  
  return 0
}

download_file ()
{
  DOWNLOAD_SERVER=$1
  DOWNLOAD_STR=$2
  TARGET_DIR=$3

  # check if file exists before downloading
  
  for CHECK_FILE in `echo "$DOWNLOAD_STR" | tr "," "\n"` ; do

    DOWNLOAD_FILE=$DOWNLOAD_SERVER/$CHECK_FILE
    WGET_RET_OK=`$WGET_COMMAND -S --spider "$DOWNLOAD_FILE" 2>&1 | grep 'HTTP/1.1 200 OK'`

    if [ ! -z "$WGET_RET_OK" ]; then
      CURRENT_FILE="$CHECK_FILE"
      FOUND=TRUE
      break
    fi
  done

  if [ ! "$FOUND" = "TRUE" ]; then
    log_error "File [$DOWNLOAD_SERVER/$DOWNLOAD_STR] does not exist"
    exit 1
  fi

  pushd . 1>/dev/null

  if [ ! -z "$TARGET_DIR" ]; then
    mkdir -p "$TARGET_DIR"
    cd "$TARGET_DIR"
  fi

  $WGET_COMMAND --progress=bar:force "$DOWNLOAD_FILE" 2>/dev/null | progressfilter

  if [ "$?" = "0" ]; then
    log_info "Successfully downloaded: $DOWNLOAD_FILE"
    popd 1>/dev/null
    return 0
  else
    log_error "File [$DOWNLOAD_FILE] not downloaded correctly"
    popd 1>/dev/null
    exit 1
  fi

  popd 1>/dev/null
  return 0
}

extract_file()
{
  FILE_NAME=$1
  TARGET_DIR=$2

  pushd .

  if [ ! -z "$TARGET_DIR" ]; then
    mkdir -p "$TARGET_DIR"  
  fi

  cd $TARGET_DIR
  tar -xvf $FILE_NAME
  
  popd
}

check_file_busy()
{
  if [ ! -e "$1" ]; then
    return 0
  fi

  TARGET_REAL_BIN=`readlink -f $1`
  FOUND_TARGETS=`lsof 2>/dev/null| awk '{print $9}' | grep "$TARGET_REAL_BIN"`

  if [ -n "$FOUND_TARGETS" ]; then
    return 1
  else
    return 0
  fi
}

install_file()
{
  SOURCE_FILE=$1
  TARGET_FILE=$2
  OWNER=$3
  GROUP=$4
  PERMS=$5

  if [ ! -r "$SOURCE_FILE" ]; then
    echo "[$SOURCE_FILE] Can not read source file"
    return 1
  fi

  if [ -e "$TARGET_FILE" ]; then

    cmp -s "$SOURCE_FILE" "$TARGET_FILE"
    if [ $? -eq 0 ]; then
      echo "[$TARGET_FILE] File did not change -- No update needed"
      return 0
    fi

    if [ ! -w "$TARGET_FILE" ]; then
      echo "[$TARGET_FILE] Can not update binary -- No write permissions"
      return 1
    fi

    check_file_busy "$TARGET_FILE"

    if [ $? -eq 1 ]; then
      echo "[$TARGET_FILE] Error - Can not update file -- Binary in use"
      return 1
    fi
  fi
  
  cp -f "$SOURCE_FILE" "$TARGET_FILE"
 
  if [ ! -z "$OWNER" ]; then
    chown $OWNER:$GROUP "$TARGET_FILE"
  fi

  if [ ! -z "$PERMS" ]; then
    chmod "$PERMS" "$TARGET_FILE"
  fi

  echo "[$TARGET_FILE] copied"
}

remove_file ()
{
  if [ -z "$1" ]; then
    return 1
  fi

  if [ ! -e "$1" ]; then
    return 2
  fi
  
  rm -rf "$1"
  return 0
}

setup_bash_configs()
{
  echo export LOTUS="$PATH_DOMINO" >> /root/.bashrc
  echo export DOMINO_PRE_STARTUP_SCRIPT="$PATH_CONFIGS/domino/prestartup.sh" >> /root/.bashrc
  echo export DOMINO_USE_JAVA_CONTROLLER="yes" >> /root/.bashrc
  echo export DOMINO_CONFIGURED="yes" >> /root/.bashrc
}

PATH_CONFIGS=/fbyte/docker
PATH_DOMINO=/opt/ibm/domino
PATH_DOMINO_DATA=/local/notesdata
PATH_SOFTWARE=/software
PATH_LOG_INSTALL_DOMINO="/fbyte/docker/install_domino.log"
PATH_LOG_INSTALL_DOMINO_FP="/fbyte/docker/install_domino_fp.log"
PATH_LOG_INSTALL_DOMINO_HF="/fbyte/docker/install_domino_hf.log"

WGET_COMMAND="wget --connect-timeout=10 --tries=1"

useradd notes -U -m
setup_bash_configs

create_directory /local root root 777 
create_directory /local/notesdata root root 777
create_directory /local/translog root root 777
create_directory /local/daos root root 777
create_directory /local/nif root root 777
create_directory /local/ft root root 777

# Set security limits for pam modules (su needs it)
echo >> /etc/security/limits.conf
echo '# -- Begin Changes Domino --' >> /etc/security/limits.conf
echo '* soft nofile 65535' >> /etc/security/limits.conf
echo '* hard nofile 65535' >> /etc/security/limits.conf
echo '# -- End Changes Domino --' >> /etc/security/limits.conf

echo
echo "Downloading files ..."
echo


download_file "$SOFTWARE_REPOSITORY_URL" "domino/Domino_10.0.1_Linux64_English.tar" "$PATH_SOFTWARE/"
download_file "$SOFTWARE_REPOSITORY_URL" "domino/Domino_10.0.1FP6_Linux64.tar" $PATH_SOFTWARE
download_file "$SOFTWARE_REPOSITORY_URL" "start_script_331.tar" $PATH_SOFTWARE

echo
echo "Extracting files ..."
echo
extract_file "$PATH_SOFTWARE/Domino_10.0.1_Linux64_English.tar" "$PATH_SOFTWARE/domino"
extract_file "$PATH_SOFTWARE/Domino_10.0.1FP6_Linux64.tar" "$PATH_SOFTWARE/domino_fp"
extract_file "$PATH_SOFTWARE/start_script_331.tar" /

echo
echo "Running Domino Silent Install -- This takes a while ..."
echo

$PATH_SOFTWARE/domino/linux64/domino/install -silent -options "/fbyte/docker/domino/domino10_response.dat"
$PATH_SOFTWARE/domino_fp/linux64/domino/install  -script $PATH_SOFTWARE/domino_fp/linux64/domino/script.dat

echo
echo "Install scripts and tools ..."
echo
/start_script/install_script
install_file "$PATH_CONFIGS/domino/docker_prestart.sh" "/docker_prestart.sh" root root 755
#install_file "$PATH_CONFIGS/domino/SetupProfile.pds" "$PATH_DOMINO_DATA/SetupProfile.pds" root root 644
#install_file "$PATH_CONFIGS/domino/DatabaseSigner.jar" "$PATH_DOMINO_DATA/DatabaseSigner.jar" root root 644
#install_file "$PATH_CONFIGS/domino/DominoUpdateConfig.jar" "$PATH_DOMINO_DATA/DominoUpdateConfig.jar" root root 644

ln -s /opt/ibm/domino/notes/latest/linux/dbmt /opt/ibm/domino/bin/
rm -rf $PATH_SOFTWARE

remove_file "$PATH_DOMINO/notes/latest/linux/tunekrnl"
remove_file $PATH_DOMINO_DATA/tika-server.jar

chown -R notes:notes $PATH_DOMINO_DATA
