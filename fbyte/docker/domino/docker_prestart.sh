#!/bin/bash

log_info ()
{
  echo "[INFO]:  $1"
}

log_error ()
{
  echo "[ERROR]: $1"
}

setup_confuration()
{
  log_info "Setup configuration"
  
  whoami

  pushd . 1>/dev/null
  cd $PATH_DOMINO_DATA
  download_file "http://repository" "configuration.json" "$PATH_DOMINO_DATA"
  echo "CMD_DOMINO_JAVA:" $CMD_DOMINO_JAVA
  echo "CMD_DOPATH_DOMINO_DATAMINO_JAVA:" $PATH_DOMINO_DATA
  if [ -e "$PATH_DOMINO_DATA/configuration.json" ]; then
    $CMD_DOMINO_JAVA -jar /fbyte/docker/domino/DominoUpdateConfig.jar "$PATH_DOMINO_DATA/configuration.json"
  else
    echo "ConfigFile [$PATH_DOMINO_DATA/configuration.json] not found!"
  fi
  popd 1>/dev/null

}

setup_applications()
{
  log_info "Setup applications: $APPLICATIONS"

}

download_file ()
{
  DOWNLOAD_SERVER=$1
  DOWNLOAD_STR=$2
  TARGET_DIR=$3

  # check if file exists before downloading
  
  for CHECK_FILE in `echo "$DOWNLOAD_STR" | tr "," "\n"` ; do

    DOWNLOAD_FILE=$DOWNLOAD_SERVER/$CHECK_FILE
    WGET_RET_OK=`$WGET_COMMAND -S --spider "$DOWNLOAD_FILE" 2>&1 | grep 'HTTP/1.1 200 OK'`

    if [ ! -z "$WGET_RET_OK" ]; then
      CURRENT_FILE="$CHECK_FILE"
      FOUND=TRUE
      break
    fi
  done

  if [ ! "$FOUND" = "TRUE" ]; then
    log_error "File [$DOWNLOAD_SERVER/$DOWNLOAD_STR] does not exist"
    exit 1
  fi

  pushd . 1>/dev/null

  if [ ! -z "$TARGET_DIR" ]; then
    mkdir -p "$TARGET_DIR"
    cd "$TARGET_DIR"
  fi

  $WGET_COMMAND --progress=bar:force "$DOWNLOAD_FILE" 2>/dev/null

  if [ "$?" = "0" ]; then
    log_info "Successfully downloaded: $DOWNLOAD_FILE"
    popd 1>/dev/null
    return 0
  else
    log_error "File [$DOWNLOAD_FILE] not downloaded correctly"
    popd 1>/dev/null
    exit 1
  fi

  popd 1>/dev/null
  return 0
}


PATH_DOMINO=/opt/ibm/domino
PATH_DOMINO_DATA=/local/notesdata
PATH_DOMINO_SETTINGS=/local/notessettings
PATH_DOMINO_PROFILE=$PATH_DOMINO_DATA/SetupProfile.pds

CMD_DOMINO_PROFILE_EDITOR="$PATH_DOMINO/notes/latest/linux/java -cp cfgdomserver.jar lotus.domino.setup.DominoServerProfileEdit"
CMD_DOMINO_SERVER=$PATH_DOMINO/bin/server
CMD_DOMINO_JAVA=$PATH_DOMINO/bin/java

WGET_COMMAND="wget --connect-timeout=10 --tries=1"


if [ -z `grep -i "ServerSetup=" $PATH_DOMINO_DATA/notes.ini` ]; then
  echo
  echo "Preparing profile for silent Domino Installation ..."
  echo

  cp /fbyte/docker/domino/SetupProfile.pds $PATH_DOMINO_PROFILE

  pushd . 1>/dev/null
  cd $PATH_DOMINO/notes/latest/linux/
  [ ! -z "$AdminFirstName" ] && $CMD_DOMINO_PROFILE_EDITOR -AdminFirstName $AdminFirstName $PATH_DOMINO_PROFILE
  [ ! -z "$AdminIDFile" ] && $CMD_DOMINO_PROFILE_EDITOR -AdminIDFile $AdminIDFile $PATH_DOMINO_PROFILE
  [ ! -z "$AdminLastName" ] && $CMD_DOMINO_PROFILE_EDITOR -AdminLastName $AdminLastName $PATH_DOMINO_PROFILE
  [ ! -z "$AdminMiddleName" ] && $CMD_DOMINO_PROFILE_EDITOR -AdminMiddleName $AdminMiddleName $PATH_DOMINO_PROFILE
  [ ! -z "$AdminPassword" ] && $CMD_DOMINO_PROFILE_EDITOR -AdminPassword $AdminPassword $PATH_DOMINO_PROFILE
  [ ! -z "$CountryCode" ] && $CMD_DOMINO_PROFILE_EDITOR -CountryCode $CountryCode $PATH_DOMINO_PROFILE
  [ ! -z "$DominoDomainName" ] && $CMD_DOMINO_PROFILE_EDITOR -DominoDomainName $DominoDomainName $PATH_DOMINO_PROFILE
  [ ! -z "$HostName" ] && $CMD_DOMINO_PROFILE_EDITOR -HostName $HostName $PATH_DOMINO_PROFILE
  [ ! -z "$OrgUnitIDFile" ] && $CMD_DOMINO_PROFILE_EDITOR -OrgUnitIDFile $OrgUnitIDFile $PATH_DOMINO_PROFILE
  [ ! -z "$OrgUnitName" ] && $CMD_DOMINO_PROFILE_EDITOR -OrgUnitName $OrgUnitName $PATH_DOMINO_PROFILE
  [ ! -z "$OrgUnitPassword" ] && $CMD_DOMINO_PROFILE_EDITOR -OrgUnitPassword $OrgUnitPassword $PATH_DOMINO_PROFILE
  [ ! -z "$OrganizationIDFile" ] && $CMD_DOMINO_PROFILE_EDITOR -OrganizationIDFile $OrganizationIDFile $PATH_DOMINO_PROFILE
  [ ! -z "$OrganizationName" ] && $CMD_DOMINO_PROFILE_EDITOR -OrganizationName $OrganizationName $PATH_DOMINO_PROFILE
  [ ! -z "$OrganizationPassword" ] && $CMD_DOMINO_PROFILE_EDITOR -OrganizationPassword $OrganizationPassword $PATH_DOMINO_PROFILE
  [ ! -z "$OtherDirectoryServerAddress" ] && $CMD_DOMINO_PROFILE_EDITOR -OtherDirectoryServerAddress $OtherDirectoryServerAddress $PATH_DOMINO_PROFILE
  [ ! -z "$OtherDirectoryServerName" ] && $CMD_DOMINO_PROFILE_EDITOR -OtherDirectoryServerName $OtherDirectoryServerName $PATH_DOMINO_PROFILE
  [ ! -z "$ServerIDFile" ] && $CMD_DOMINO_PROFILE_EDITOR -ServerIDFile $ServerIDFile $PATH_DOMINO_PROFILE
  [ ! -z "$ServerName" ] && $CMD_DOMINO_PROFILE_EDITOR -ServerName $ServerName $PATH_DOMINO_PROFILE
  [ ! -z "$SystemDatabasePath" ] && $CMD_DOMINO_PROFILE_EDITOR -SystemDatabasePath $SystemDatabasePath $PATH_DOMINO_PROFILE
  [ ! -z "$ServerPassword" ] && $CMD_DOMINO_PROFILE_EDITOR -ServerPassword $ServerPassword $PATH_DOMINO_PROFILE

  echo 
  $CMD_DOMINO_PROFILE_EDITOR -dump $PATH_DOMINO_PROFILE
  popd 1>/dev/null

  pushd . 1>/dev/null
  cd $PATH_DOMINO_DATA
  $CMD_DOMINO_SERVER -silent $PATH_DOMINO_PROFILE
  popd 1>/dev/null

  if [ -z `grep -i "ServerSetup=" $PATH_DOMINO_DATA/notes.ini` ]; then
    echo "Silent Server Setup unsuccessful -- check [$DOMINO_DATA_PATH/setuplog.txt] for details"
  else
    echo "Silent Server Setup done"
  fi
fi

setup_applications
setup_confuration

unset AdminFirstName
unset AdminIDFile
unset AdminLastName
unset AdminMiddleName
unset AdminPassword
unset CountryCode
unset DominoDomainName
unset HostName
unset OrgUnitIDFile
unset OrgUnitName
unset OrgUnitPassword
unset OrganizationIDFile
unset OrganizationName
unset OrganizationPassword
unset OtherDirectoryServerAddress
unset OtherDirectoryServerName
unset ServerIDFile
unset ConfigFile 
unset SafeIDFile
unset ServerName
unset ServerPassword
unset SystemDatabasePath

history -c

exit 0