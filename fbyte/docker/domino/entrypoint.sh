#!/bin/bash

domino_is_running ()
{
  DOMINO_RUNNING=`ps -fu notes | grep "$PATH_DOMINO/notes" | grep "server" | grep -v " -jc"`
  if [ -n "$DOMINO_RUNNING" ]; then
    return 1
  else
    return 0
  fi

  return 0
}

get_notes_ini_var () 
{ 
  # $1 = filename 
  # $2 = ini.variable 

  ret_ini_var="" 
  if [ -z "$1" ]; then 
    return 0 
  fi 

  if [ -z "$2" ]; then 
    return 0 
  fi 

  ret_ini_var=`awk -F '(=| =)' -v SEARCH_STR="$2" '{if (tolower($1) == tolower(SEARCH_STR)) print $2}' $1 | xargs` 
  return 0 
}

console_start ()
{
  get_notes_ini_var "$PATH_DOMINO_DATA/notes.ini" "DominoControllerCurrentLog"
  while [ -z $ret_ini_var ];
  do
    sleep 1
    get_notes_ini_var "$PATH_DOMINO_DATA/notes.ini" "DominoControllerCurrentLog"
  done

  TAIL_FILE=$PATH_DOMINO_DATA/$ret_ini_var

  echo 
  echo "--- Live Console ---"
  echo 
  echo "To close console, always type 'close' or 'stop'."
  echo 
  echo 
  sleep 2

  $CMD_TAIL -f $TAIL_FILE &

  MONITOR_PID=$!
  trap "console_stop" 1 2 3 4 6 9 13 15 17 19 23

  # avoid expansion of wildcards like star
  set -f 

  var=dummy
  while true
  do
    lastvar=$var
    var="PIPE-DEAD"
    read var
    
    if [ "$var" = "PIPE-DEAD" ]; then
      echo "Terminating live console - Input Pipe is dead!"
      console_stop
      exit 0
    fi

    if [ "$var" = "exit" ]; then
      console_stop
      return 0
    elif [ "$var" = "quit" ]; then
      console_stop
      return 0
    elif [ "$var" = "e" ]; then
      console_stop
      return 0
    elif [ "$var" = "q" ]; then
      console_stop
      return 0
    elif [ "$var" = "stop" ]; then
      console_stop
      return 0
    elif [ "$var" = "close" ]; then
      console_stop
      return 0
    else
      if [ ! -z "$var" ]; then
        cd "$PATH_DOMINO_DATA"
        su notes -c "$CMD_DOMINO_SERVER -c '$var'"
      fi
    fi
  done
  
  return 0
}

console_stop ()
{
  

  domino_is_running
  while [ $? -eq 1 ];
  do
    sleep 1
    domino_is_running
  done
  echo "SERVER shutdown"

  if [ ! -z "$MONITOR_PID" ]; then
    kill -9 $MONITOR_PID > /dev/null 2>&1
    echo "Live Console closed."
    echo
    echo
    exit 0
  fi
}

setup_confuration()
{
  if [ -e "$PATH_DOMINO_TEMPLATES/configuration.json" ]; then
    $CMD_DOMINO_JAVA -jar $PATH_DOMINO_DATA/DominoUpdateConfig.jar "$PATH_DOMINO_TEMPLATES/configuration.json"
  else
    echo "ConfigFile [$PATH_DOMINO_TEMPLATES/configuration.json] not found!"
  fi
}

start_server ()
{
  export DOMINO_USE_JAVA_CONTROLLER="yes"
  cd $PATH_DOMINO_DATA
  su notes -c "$CMD_DOMINO_SERVER -jc -c" &
}

stop_server ()
{
  domino_is_running
  if [ $? -eq 1 ]; then
    su notes -c "$CMD_DOMINO_SERVER -c 'quit'"
  fi
  console_stop
  exit 0
}

PATH_DOMINO=/opt/ibm/domino
PATH_DOMINO_DATA=/local/notesdata
PATH_DOMINO_TEMPLATES=/local/templates
PATH_DOMINO_PROFILE=$PATH_DOMINO_DATA/SetupProfile.pds

CMD_DOMINO_PROFILE_EDITOR="$PATH_DOMINO/notes/latest/linux/java -cp cfgdomserver.jar lotus.domino.setup.DominoServerProfileEdit"
CMD_TAIL=tail
CMD_DOMINO_SERVER=$PATH_DOMINO/bin/server
CMD_DOMINO_JAVA=$PATH_DOMINO/bin/server

if [ -z `grep -i "ServerSetup=" $PATH_DOMINO_DATA/notes.ini` ]; then
  echo
  echo "Preparing profile for silent Domino Installation ..."
  echo
  
  pushd . 1>/dev/null
  cd $PATH_DOMINO/notes/latest/linux/
  [ ! -z "$AdminFirstName" ] && $CMD_DOMINO_PROFILE_EDITOR -AdminFirstName $AdminFirstName $PATH_DOMINO_PROFILE
  [ ! -z "$AdminIDFile" ] && $CMD_DOMINO_PROFILE_EDITOR -AdminIDFile $AdminIDFile $PATH_DOMINO_PROFILE
  [ ! -z "$AdminLastName" ] && $CMD_DOMINO_PROFILE_EDITOR -AdminLastName $AdminLastName $PATH_DOMINO_PROFILE
  [ ! -z "$AdminMiddleName" ] && $CMD_DOMINO_PROFILE_EDITOR -AdminMiddleName $AdminMiddleName $PATH_DOMINO_PROFILE
  [ ! -z "$AdminPassword" ] && $CMD_DOMINO_PROFILE_EDITOR -AdminPassword $AdminPassword $PATH_DOMINO_PROFILE
  [ ! -z "$CountryCode" ] && $CMD_DOMINO_PROFILE_EDITOR -CountryCode $CountryCode $PATH_DOMINO_PROFILE
  [ ! -z "$DominoDomainName" ] && $CMD_DOMINO_PROFILE_EDITOR -DominoDomainName $DominoDomainName $PATH_DOMINO_PROFILE
  [ ! -z "$HostName" ] && $CMD_DOMINO_PROFILE_EDITOR -HostName $HostName $PATH_DOMINO_PROFILE
  [ ! -z "$OrgUnitIDFile" ] && $CMD_DOMINO_PROFILE_EDITOR -OrgUnitIDFile $OrgUnitIDFile $PATH_DOMINO_PROFILE
  [ ! -z "$OrgUnitName" ] && $CMD_DOMINO_PROFILE_EDITOR -OrgUnitName $OrgUnitName $PATH_DOMINO_PROFILE
  [ ! -z "$OrgUnitPassword" ] && $CMD_DOMINO_PROFILE_EDITOR -OrgUnitPassword $OrgUnitPassword $PATH_DOMINO_PROFILE
  [ ! -z "$OrganizationIDFile" ] && $CMD_DOMINO_PROFILE_EDITOR -OrganizationIDFile $OrganizationIDFile $PATH_DOMINO_PROFILE
  [ ! -z "$OrganizationName" ] && $CMD_DOMINO_PROFILE_EDITOR -OrganizationName $OrganizationName $PATH_DOMINO_PROFILE
  [ ! -z "$OrganizationPassword" ] && $CMD_DOMINO_PROFILE_EDITOR -OrganizationPassword $OrganizationPassword $PATH_DOMINO_PROFILE
  [ ! -z "$OtherDirectoryServerAddress" ] && $CMD_DOMINO_PROFILE_EDITOR -OtherDirectoryServerAddress $OtherDirectoryServerAddress $PATH_DOMINO_PROFILE
  [ ! -z "$OtherDirectoryServerName" ] && $CMD_DOMINO_PROFILE_EDITOR -OtherDirectoryServerName $OtherDirectoryServerName $PATH_DOMINO_PROFILE
  [ ! -z "$ServerIDFile" ] && $CMD_DOMINO_PROFILE_EDITOR -ServerIDFile $ServerIDFile $PATH_DOMINO_PROFILE
  [ ! -z "$ServerName" ] && $CMD_DOMINO_PROFILE_EDITOR -ServerName $ServerName $PATH_DOMINO_PROFILE
  [ ! -z "$SystemDatabasePath" ] && $CMD_DOMINO_PROFILE_EDITOR -SystemDatabasePath $SystemDatabasePath $PATH_DOMINO_PROFILE
  [ ! -z "$ServerPassword" ] && $CMD_DOMINO_PROFILE_EDITOR -ServerPassword $ServerPassword $PATH_DOMINO_PROFILE

  echo 
  $CMD_DOMINO_PROFILE_EDITOR -dump $PATH_DOMINO_PROFILE
  popd 1>/dev/null

  pushd . 1>/dev/null
  cd $PATH_DOMINO_DATA
  su notes -c "$CMD_DOMINO_SERVER -silent $PATH_DOMINO_PROFILE"
  popd 1>/dev/null

  if [ -z `grep -i "ServerSetup=" $PATH_DOMINO_DATA/notes.ini` ]; then
    echo "Silent Server Setup unsuccessful -- check [$DOMINO_DATA_PATH/setuplog.txt] for details"
  else
    echo "Silent Server Setup done"
  fi
fi

trap "stop_server" 1 2 3 4 6 9 13 15 17 19 23

setup_confuration
start_server


while true
do
  sleep 1
done

exit 0