Контейнер FByte.Platform на HCL Domino
---
Данный репозиторий предназначен для разворачивания FByte.Platform на базе сервера приложений HCL Domino.  
К сожалению, HCL не публикует образ сервер на Docker Hub, поэтому придется создать свой образ, который находиться в вашем локальном репозитории образов.

>Примечание!  
>Данный репозиторий создан на основе репозитория [https://github.com/IBM/domino-docker](https://github.com/IBM/domino-docker).  
>Для лучшего понимания я решил удалить из него все, что посчитал лишним, и слегка модифицировал под возможности FByte.Platform.

Для начала работы необходимо:  
1.  Установить и настроить Docker.  
2.  Скачать репозиторий.  
3.  Разместить файл установки сервера HCL Domino (имя файла Domino_10.0.1_Linux64_English.tar) в папке /software/domino.  
4.  Разместить файл установки сервера Fix Pack 6 (имя файла Domino_10.0.1FP6_Linux64.tar) в папке /software/domino.  
5.  Разместить файл установки скриптов от [Daniel Nashed](https://www.nashcom.de/nshweb/pages/startscript.htm) (имя файла start_script_331.tar) в папке /software.

Создание образа:  
1.  Открыть терминал и перейти в каталог репозитория.  
2.  Запустить сборку репозитория командой ./build.sh  
3.  Сборка образа займет около 10 минут...  

Запуск контейнера:  
1.  Открыть терминал.  
2.  Запустить контейнер командой:  
```bash
docker run -it -e "ServerName=Server1" \
    -e "OrganizationName=MyOrg" \
    -e "AdminFirstName=Ramazan" \
    -e "AdminLastName=Salpagarov" \
    -e "AdminPassword=12345" \
    -p 80:80 \
    -p 1352:1352 \
    -v dominodata:/local/notesdata \
    --stop-timeout=60 \
    --name server1 \
    freshbyte/domino:10.0.1FP6
```

Если вы хотите изменить нахождения файлов установки, то это можно сделать в файле build.sh - нужно изменить переменную SOFTWARE_REPOSITORY_PATH

Полный список параметров сервера, которые вы можете передавать, можно найти тут https://github.com/IBM/domino-docker/blob/master/docs/run-variables.md

